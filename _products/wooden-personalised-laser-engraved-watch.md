---
title: "Wooden personalised laser engraved watch"
price: "29.00"
original_price: "44.00"
sku: WOODWATCH
options:
  - material:
    name: material
    type: select
    label: Material
    sku: ['SW', 'BA']
    select:
      - Sandalwood
      - Bamboo
  - font:
    name: font
    type: select
    label: Font
    select:
      - "1 - Lobster"
      - "2 - Abel"
      - "3 - Architects Daughter"
      - "4 - Over the Rainbow"
      - "5 - Open Sans"
      - "6 - Smythe"
      - "7 - Abhaya Libre"
      - "8 - Post No Bills Colombo"
      - "9 - Smokum"
      - "10 - Sancreek"
  - engrave_text:
    name: engrave_text
    type: text
    label: Text to engrave

image: img/woodwatch01-1.jpg
description: "This luxury wooden wrist watch is coming with a Japan quartz movement and the housing is entirely handmade, makes it a perfect gift for any occasion. Be it anniversary, father's day, Valentine's day or birthday."
keywords: wooden watch, wrist watch, mens watch, men watch, timepiece, bamboo watch, engraved watch, personalised watch, laser engraved, keepsake watch, sandalwood, bamboo, wood watch
gallery:
  - img/woodwatch02-2.jpg
  - img/woodwatch01-3.jpg
  - img/woodwatch01-4.jpg
  - img/woodwatch01-5.jpg
  - img/woodwatch01-6.jpg
  - img/woodwatch02-7.jpg
  - img/woodwatch02-8.jpg
  - img/fonts.png
tags:
  - home
---

   - Handmade item
   - Style: Boho
   - Can be personalised: Yes
   - Readout: Analog
   - Power: Battery
   - Band material: Leather
   - Case material: Wood
   - Made to order

This unique design wooden wrist watch is light on the hand. Looks good with a suit or jeans as well.

This luxury watch is coming with a Japan quartz movement and the housing is entirely handmade, makes it a perfect gift for any occasion. Be it anniversary, father's day, Valentine's day or birthday.

It looks understated beauty, comfort and simplicity. It is very quiet! No loud ticking.
This wooden watch is light, so lightweight that you almost forget it's on your wrist!
The special Genuine Leather Strap provides you a smell-less, flexible and soft touching experience.
Reliable high quality Japanese quartz movement with 24 month warranty. Each Watch is guaranteed to provide you a long-lasting enjoyment.

I can engrave anything on the back, please message me the chosen message during checkout.

Chose from 10 font or let me know the Google Webfont name you would like to use, simply message me during checkout the name of the font. You can select from thousand fonts using the Google Font website: https://fonts.google.com/

Sandalwood or Bamboo wood material. This is a hard wearing, beautiful timepiece made with love and passion.

For the darker watch, choose the Sandalwood option. For the lighter colour, choose Bamboo.

I ship Worldwide!
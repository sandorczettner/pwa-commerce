---
title: "Clear resin bangle with Scottish fern"
price: "19.99"
sku: BANG001
image: img/bang001-1.jpg
description: "The fern that's embedded in this clear epoxy resin bangle was collected in the Scottish Highlands. This luxury bangle is preserving the best of the million shades of green seen around this beautiful part of our globe."
keywords: "resin bangle, boho bangle, natural jewellery, fern bangle, gift for her"
tags:
  - home
---

The fern that's embedded in this clear epoxy resin bangle was collected in the Scottish Highlands. This luxury bangle is preserving the best of the million shades of green seen around this beautiful part of our globe.

Inner Diameter: 65 mm

With this purchase you will receive the exact bangle in the photo; a one-of-a-kind piece that is a beautiful bangle and one you can wear proudly in the knowledge that no one is wearing the same piece.
---
title: "Castle moss earring"
price: "18.60"
sku: EARM001
image: img/earm001-1.jpg
description: "Beautiful Scottish castle moss in a tiny flask earring. Attached to a sterling silver hook. This lightweight earring would make an unique gift for her."
keywords: "resin earring, boho earring, natural jewellery, moss earring, sterling silver earring"
tags:
  - home
gallery:
  - img/earm001-2.jpg
---

A stunning glass and resin earring from Dora's Jewellery, handmade in Scotland using moss grown around the historic Birkwood Castle. This lightweight earring would made an unique gift for her.

This is a made to order earring. The picture is to show the shape, colour and style of the earring you are buying. The item you receive may differ ever so slightly from this image. I work as close to stock image as possible to replicate the very same loveliness.

Materials: Glass, moss, resin. The resin is beautifully clear. The dried moss encapsulated in the resin is slightly see through.
---
title: "Personalised Engraved Wallet"
price: "28.00"
sku: WALLET01
options:
  - style:
    name: style
    type: select
    label: Style
    select:
      - "1 - Best Dad in the world"
      - "2 - Daddy we love you (Heart)"
      - "3 - Daddy we love you (2 Heart)"
      - "4 - To daddy"
      - "5 - Guess what"
      - "6 - We have a hero"
  - engrave_text:
    name: engrave_text
    type: text
    label: Name to engrave

image: img/wallet01-1.jpg
description: "Laser engraved, made to order personalized real leather wallet. It's perfect for someone looking for a unique gift for fathers, husbands, grandfathers, boyfriends, groomsmen."
keywords: leather wallet, engraved wallet, personalised wallet, groomsmen, gift for boyfriend, gift for men
gallery:
  - img/wallet01-2.jpg
  - img/wallet01-3.jpg
  - img/wallet01-4.jpg
  - img/wallet01-5.jpg
  - img/wallet01-6.jpg
  - img/wallet01-7.jpg
  - img/wallet-pattern.png
tags:
---

Laser engraved, made to order personalized real leather wallet.
It's perfect for someone looking for a unique gift for fathers, husbands, grandfathers, boyfriends, groomsmen.

This hard wearing luxury leather wallet is the perfect gift for any man. I can engrave anything on the wallet, just let me know at checkout. The maximum size of the engraving is 38mmx38mm.

If you would like to further personalise this item with initials engraved on the outside, just let me know. I can engrave up to 4 initials on the bottom right corner on the front and around 20 characters in the inside.

  - 3 x credit card slots, 1 x transparent window, 2 x back pockets
  - 2 x money pockets
  - 1 x zipper pocket

Colour: dark brown

Very Durable and made to last. Handmade and hand stiched. Rustic and beautiful. This wallet is made from real cowhide therefore each piece is slightly different and may have markings from the manufacturing process. It's aging to a beautiful shiny wallet as you wear it.

I ship worldwide from the UK.
---
title: "Walnut and ultramarine resin ring"
price: "22.50"
sku: WNUTRING-N
image: img/wnutring-n-1.jpg
description: "Rich walnut wood and transparent ultramarine trendy ring for any occasion"
keywords: "resin ring, boho ring, resin and wood ring. natural jewellery, ultramarine ring"
tags:
  - home
gallery:
  - img/wnutring-n.jpg
---

This trendy ring is made with walnut wood and epoxy resin with ultramarine pigment. Ring has size N (inner diameter is 17.1mm). All the materials I used to make this beautiful ring are eco-friendly and non-toxic. Every jewellery I sell is exclusive and handmade.

Thanks to the wax I used to polish this ring, the wood is waterproof, but please do not keep it in water for longer periods. Thank to the high quality epoxy resin I used this ring will stay shiny.

This specific ring is not sizeable but I can made one of the size you need. Please note that there's no identical ring as there's no identical wood structure. Every ring is one of a kind and unique, but I will try to stay as close to the stock image as possible.
---
title: Resin Jewelry Trends This Spring
description: In 2018 Spring, resin jewellery takes centre stage as we see an assortment of designs incorporating this durable material. From bracelets to pendants, clear to coloured, resin jewellery will be a key element in jewelry this years's spring.
keywords: Clear resin jewellery, coloured resin jewellery, 2018 spring trends
layout: post
image: img/flasks.jpg
---

In 2018 Spring, resin jewellery takes centre stage as we see an assortment of designs incorporating this durable material. From bracelets to pendants, clear to coloured, resin jewellery will be a key element in jewellery this years spring.

It is impossible to characterize jewellery trends 2018 in a single paragraph. After all, my favourite jewellery accessories get to their new level. In 2018, you can forget to take a wallet with you, but forgetting bracelet or earrings is considered bad taste. I know how hard it is to find a piece that's not mass-produced but one of a kind jewellery. The market is too saturated with Chinese products. These days everything is made in China, even if you don't suspect it is. It's not a surprise, after all: Chinese companies are getting better achieving better quality control and manufacturing something in China will always be cheaper. Just look at your iPhone, your telly or even your microwave. One common in these things is the country of manufacture.

Back to the trends, it's not hard to tell that pendants will grow to the breasts and in some cases, lower. Clear transparent earring pieces will be a usual sight. Mimicking organic materials around your wrist with a clear or coloured bracelet with a leave or feather in it will be considered trendy.

Chokers will still remain fashionable, you just need to add a plate with a slogan. Furthermore, few of your finger needs to wear one or more rings.
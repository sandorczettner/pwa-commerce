---
title: All about resin
description: There are three types of resin commonly encountered in jewellery making. Polyester resin, Polyurethane resin, Epoxy resin
keywords: Polyester resin, Polyurethane resin, Epoxy resin, resin for jewellery making, how to make resin jewellery
layout: post
image: uploads/resin.jpg
---

When it comes to resin, there are so many of them. I'm going to try to show them all here with pros and cons.

All synthetic resins are a two component sytem, consisting of the resin and the hardener or catalyst. Without the hardener, the resin stays fluid.

There are three types of resin commonly encountered in jewellery making:

## Polyester resin

May also be referred to as casting resin. It's a cheap material compared to epoxy resin with it's drawbacks. usually can be distinguished by the amount of activator it needs to cure. It needs only a few drops, definitely not 2:1 or 3:1 ratio.

When the activator or catalyst is mixed to the resin, it's pot time is only minutes. Within a few hours, it's completely cured and can be removed from the mold.

It's fumes during drying are very dangerous. Polyester resin have a noxious smell. It requires a respirator and good ventilation to work with this material. The fumes are bad for the environment.

Pros:  Cures to a hard finish. Easy to sand and polish. Scratches can be buffed again.

Cons:  Not UV stable, polyester resin will eventually yellow by time. It may easily break if dropped on a hard floor.

## Polyurethane resin

Polyurethanes get expensive with an increase in clarity and for water clear versions. To cast, it requires a vacuum chamber and polyurethane is very sensitive to moisture while it's curing. Depending on the contents of polyurethane resin, it might be dangerous, so a respirator might be required.

Pros: Very quick curing.

Cons: Polyurethane resin needs expensive tools to work with, does not accept embedded objects with water content: flowers, wood bark, etc.

##  Epoxy resin

Epoxy resin is the king of resins when it comes to jewellery making. It's getting more expensive when it's water clear and takes long time to fully cure: it could take a few hours to days. Epoxy resin is safe to work with, does not produce dangerous fumes, but ventilation is always a good idea. Epoxy resin jewellery is hard wearing and scratch resistant, making it labour intensive to buff and polish. The end result however is extremely shiny surface.

Pros: Excellent clarity, hard wearing, almost unbreakable.

Cons: Long curing time, expensive.

## Summary

As a summary, the most ideal resin for jewellery making is the clear epoxy resin. It can enclose almost anything including flowers, leaves. Epoxy resin jewellery is relatively easy to make.
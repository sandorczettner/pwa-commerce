---
title: Jewellery Trends for Spring/Summer 2018
description: 2018 is already here and we’re totally excited to get into our resolutions and get inspirations for all our jewellery goals this year. I’m sharing the biggest upcoming trends I think will happen so you can get the latest looks before everyone else.
keywords: pendants, charm, bangle, Epoxy resin, resin for jewellery making, jewellery trends, trendy earring
layout: post
image: uploads/spring-summer.jpg
---

2018 is already here and we're totally excited to get into our resolutions and get inspirations for all our jewellery goals this year. I'm sharing the biggest upcoming trends I think will happen so you can get the latest looks before everyone else.

One show that's most likely making a strong statement is the Spring-Summer 2018 Ready-to-Wear CHANEL Show.

It seems Chanel like plastic clothing everywhere. Chanel's Traditional icons: pearls, camellias are also respected in this years show. With a dash of urban practicality. Big charms, pendants and plastic rings are just a few of the ways you can experiment with this trend in the current year.

Watch the full show here:

<iframe width="560" height="315" src="https://www.youtube.com/embed/jmPD4BSyRUE?rel=0&amp;showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
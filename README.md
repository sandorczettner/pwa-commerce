# PWA Commerce

This is a simple serverless ecommerce engine using Parse for stock data, PayPal for payment method and Jekyll to generate html and manage products. It was made for my amusement to better understand serverless environments and show what is possible with PWA technologies. Altough it's a working ecommerce site, it's not recommended it to use it on production yet. My wife made a few sales with it before I finally switched Her shop to Magento.

## Features

- PayPal payment
- Configurable product options (dropdown, free text)
- Customer registration
- Address book
- Fully functioning cart entirely in localstorage
- Purchasing an item decreases the stock
- Live price and stock updates from Parse

## Usage

Set up a Parse Server or use an online service. It's sold for $5/month usually. I recommend [back4app.com](https://back4app.com), because it's free until lots of traffic hits your site.

Download this repository.

Edit \_config.yml and replace Parse and PayPal details.

Run to try it locally:

```
bundle install
bundle exec jekyll serve
```

Upload your site to somewhere in the cloud. It's static content, so it's super cheap to host. I use [Netlify](https://www.netlify.com/)

## Demo

[https://pwa-commerce.netlify.com/](https://pwa-commerce.netlify.com/)

(If you accidentally ordered something from there, write Dora for a refund)

## Screenshots

Simple product:

![simple product](https://pwa-commerce.netlify.com/img/demo/simple-product.jpg)

Configurable product:

![configurable product](https://pwa-commerce.netlify.com/img/demo/configurable-product.png)

Shopping cart:

![shopping cart](https://pwa-commerce.netlify.com/img/demo/shopping-cart.jpg)

Mobile version:

![mobile version](https://pwa-commerce.netlify.com/img/demo/mobile-version.jpg)

## Code of Conduct

This project adheres to No Code of Conduct.  We are all adults.  We accept anyone's contributions.  Nothing else matters.

For more information please visit the [No Code of Conduct](https://github.com/domgetter/NCoC) homepage.

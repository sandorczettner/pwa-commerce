---
title: How to care for your resin jewellery and FAQ
description: Care tips for your resin epoxy jewellery. How to care for your epoxy resin pendants, earrings, bracelets.
keywords: epoxy resin jewellery care, wooden jewellery care, resin pendant care
layout: page
---

It looks like glass. Is it fragile?
  : Fortunately only it's transparency is common with glass. Epoxy resin is not fragile at all.

Does the flower inside it fade?
  : By casting the flower in the resin, it's oxidisation stops as it has no longer access to oxygen. It will keep it's same lovely colour.

How resin changes by time?
  : I use the highest quality epoxy resin, which is UV stable. Once it's set, it does not deform or yellow.

How to care for resin jewellery?
  : This type of resin is polishing itself on the human skin, so the best care is to wear it. It can be easily cleaned by wiping it down with a moist cloth. Avoid contact with cleaning solutions and alcohol.

How to care for wooden jewellery?
  : Exposed wooden parts can be renewed by wiping the wood with raw linseed oil and wiping down the excess oil. Pure bees wax is also an option, but make sure it's not causing allergic reaction.
"use strict";

define("doracart", [
        "jquery",
        "knockout"
        ],
function($, ko){

// Thanks for: https://github.com/robconery/knockout-cart
//custom binding for currency formatting (wrapper to text binding)
ko.bindingHandlers.currency = {
    update: function(element, valueAccessor) {
        //unwrap the amount (could be observable or not)
        var amount = parseFloat(ko.utils.unwrapObservable(valueAccessor())) || 0;

        //could set the innerText/textContent directly or use $.text(), but we will just let the real text binding handle it by passing it our formatted text
        var newValueAccessor = function() {
            return "£" + amount.toFixed(2);
        };

        //call real text binding
        ko.bindingHandlers.text.update(element, newValueAccessor);
    }
};

//wrapper to click binding.  call handler with object from data- attributes
ko.bindingHandlers.clickWithData = {
  init: function(element, valueAccessor, allBindings, vm, context) {
    var data = $(element).data();
    // var value = ko.unwrap(valueAccessor());
    data.options = vm.product.selection()
    var boundHandler = valueAccessor().bind(this, data);
    delete data.bind; //remove the data-bind attribute's data
    ko.applyBindingsToNode(element, { click: boundHandler });
  },
  update: function(element, valueAccessor, allBindings, vm, context) {
    var data = $(element).data();
    var value = ko.unwrap(valueAccessor());
    data.options = vm.product.selection()
    var boundHandler = valueAccessor().bind(this, data);
    delete data.bind; //remove the data-bind attribute's data
    //ko.applyBindingsToNode(element, { click: boundHandler });
  }
};

//extension to replace an observable with a writeable computed that forces write to be numeric
ko.observable.fn.asPositiveInteger = function(defaultForBadValue) {
  var original = this;
  var interceptor = ko.computed({
      read: original,
      write: function(newValue) {
          var parsed = parseInt(newValue, 10);
          //if value is bad or negative, then use default
          if (isNaN(parsed) || parsed < 0) {
               parsed = defaultForBadValue;
          }
          original(parsed);
      }
   });

     //process the initial value
     interceptor(original());

  //return this new writeable computed to "stand in front of" our original observable
  return interceptor;
};

var Dora = Dora || {};
Dora.CartItem = function(options,callback) {

  var cartItem = {};
  var qty = (options.quantity || 1);
  var optionsJSON = options.options;

  cartItem.price = options.price || 0.00;

  cartItem.quantity = ko.observable(qty).asPositiveInteger(1);

  cartItem.sku = options.sku || "";
  cartItem.description = options.description || "";
  cartItem.image = options.image || "";
  cartItem.url = options.url || "";
  cartItem.options = optionsJSON || {"sku": ""};

  cartItem.sku;

  cartItem.priceInPennies = function(){
    return cartItem.price * 100;
  };

  cartItem.subtotal = ko.computed(function(){
    return cartItem.price * cartItem.quantity();
  });

  cartItem.discount = ko.computed(function(){
    // Discount logic here
    return 0;
  });

  cartItem.lineTotal = ko.computed(function() {
    return cartItem.subtotal() - cartItem.discount();
  });

  if(callback){
    cartItem.quantity.subscribe(function(quantity){
      callback.call(cartItem,cartItem);
    });
  }
  return cartItem;
}

Dora.ShippingRate = function(options,callback) {
  var shippingRate = {};

  shippingRate.code = options.code || "";
  shippingRate.country = options.country || "";
  shippingRate.method = options.method || "";
  shippingRate.price = options.price || 0.00;
  return shippingRate;
}

Dora.Cart = function(){
  var self = this;
  var storedItems = JSON.parse(localStorage.getItem("doraCart")) || [];
  var storedCheckout = JSON.parse(localStorage.getItem("doraCheckout")) || {};
  self.items = ko.observableArray();
  self.country = ko.observable("UK");
  self.shippingRates = ko.observableArray([
    new Dora.ShippingRate({price: 1.50, code: 'UK', country: "United Kingdom", method: "Standard UK Royal Mail Shipping"}),
    new Dora.ShippingRate({price: 4.40, code: 'USA', country: "United States of America", method: "International Standard Shipping to USA"}),
    new Dora.ShippingRate({price: 4.75, code: 'AU', country: "Australia", method: "International Standard Shipping to Australia"}),
    new Dora.ShippingRate({price: 5.75, code: 'AT', country: "Austria", method: "EU Standard Shipping Rate"}),
    new Dora.ShippingRate({price: 5.75, code: 'DE', country: "Germany", method: "EU Standard Shipping Rate"}),
    new Dora.ShippingRate({price: 5.75, code: 'HU', country: "Hungary", method: "EU Standard Shipping Rate"})
  ]);

  self.shippingPrice = ko.computed(function(){
    var rates = self.shippingRates();
    var country = self.country();
    for (var i = 0; i < rates.length; i++) {
      if (rates[i].code == country) {
        return rates[i].price;
      }
    }
  });
  self.shippingMethod = ko.computed(function(){
    var rates = self.shippingRates();
    var country = self.country();
    for (var i = 0; i < rates.length; i++) {
      if (rates[i].code == country) {
        return rates[i].method;
      }
    }
  });

  //remove an item if quantity is 0, passed to CartItem
  self.itemQuantityCheck = function(item) {
      if (item && item.quantity() === 0) {
          self.items.remove(item);
      }
  };

  //send the items that we load from storage through the CartItem constructor
  self.items(ko.utils.arrayMap(storedItems, function(item) {
     return Dora.CartItem(item, self.itemQuantityCheck);
  }));


  self.addItem = function(item){
    var existing = self.find(item.sku);
    var items = self.items();

    if(existing){
      existing.quantity(existing.quantity() + parseInt(item.quantity || 1, 10));
    }else{
      existing = Dora.CartItem(item,self.itemQuantityCheck);
      self.items.push(existing);
    }
    return existing;
  };

  self.rowCount = function() {
    return self.items().length;
  };

  self.remove = function(sku) {
    self.items.remove(function(item) {
      return item.sku == sku;
    });
  };

  self.removeClicked = function(item) {
    self.remove(item.sku);
  };

  self.itemCount = function() {
    var itemCount = 0;
    ko.utils.arrayForEach(self.items(),function(item){
      itemCount += item.quantity();
    });
    return itemCount;
  };

  self.skuCount = function(sku) {
    var itemCount = 0;
    ko.utils.arrayForEach(self.items(),function(item){
      if (item.sku === sku) {
        itemCount += item.quantity();
      }
    });
    return itemCount;
  };

  self.total = function() {
    var sum = 0;
    ko.utils.arrayForEach(self.items(),function(item){
      sum += item.lineTotal();
    });
    return sum;
  };

  self.empty = function(){
    self.items([]);
  };

  self.find = function(sku){
    return ko.utils.arrayFirst(self.items(),function(item){
      return item.sku === sku;
    });
  };

  //dirty tracking
  ko.computed(function(){
    localStorage.setItem("doraCart", ko.toJSON(self.items));
    localStorage.setItem("doraCheckout", ko.toJSON({
      "country": self.country
    }));
  }).extend({throttle : 1});

  self.hasItems = ko.computed(function(){
    return self.rowCount() > 0;
  });

  self.isNumber = function(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
  return self;
};

return Dora;

});
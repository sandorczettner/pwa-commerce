---
---
"use strict"; // Start of use strict

require.config({
  // Sets the js folder as the base directory for all future relative paths
  baseUrl: "/",
  // 3rd party script alias names
  paths: {
      "jquery": "vendor/jquery/jquery.min",
      "jquery-easing": "vendor/jquery-easing/jquery.easing.min",
      "fitvid": "vendor/fitvid/jquery.fitvids",
      "knockout": "vendor/knockout/knockout-3.4.2",
      "knockout-validation": "vendor/knockout/knockout.validation.min",
      "bootstrap": "vendor/bootstrap/js/bootstrap.bundle.min",
      "xzoom": "vendor/xzoom/xzoom.min",
      "parse": "vendor/parse/parse.min",
      "doracart": "js/doracart.min",
      "account": "js/account.min",
      "stock": "js/stock.min",
      "product": "js/product.min"
  },

  // Sets the configuration for your third party scripts that are not AMD compatible
  shim: {
      "bootstrap": {
        deps: ["jquery"],
        exports: "$.fn.popover"
      },
      "jquery-easing": ["jquery"],
      "fitvid": {
        deps: ["jquery"],
        exports: "$.fn.fitVids"
      },
      "knockout-validation": ["knockout"],
      "xzoom": {
        deps: ["jquery"],
        exports: "$.fn.xzoom"
      },
      "parse": {
        exports: "Parse"
      }
  },
  enforceDefine: true
});

define([
        "jquery",
        "knockout",
        "stock",
        "product",
        "doracart",
        "account",
        "parse",
        "fitvid",
        "jquery-easing",
        "bootstrap",
        "xzoom"
        ],
function($, ko, stock, product ,dora, account, Parse){

  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 48)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 54
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);

  $("#quickcart").on("show.bs.collapse", function () {
    $(window).scrollTop(0);
  });

  $(".add-to-cart button").click(function() {
    $(".cart-popover").addClass('blinking');
    setTimeout(function() {
      $(".cart-popover").removeClass("blinking");
    }, 3000);
  });

  /* calling script */
  $(".xzoom, .xzoom-gallery").xzoom({tint: '#333', Xoffset: 15});

  $(".post-page").fitVids();

  Parse.initialize("{{ site.parse.app_id }}", "{{ site.parse.js_key }}");
  Parse.serverURL = 'https://parseapi.back4app.com/';
  var currentUser = Parse.User.current();

  var cart = new dora.Cart();

  var ViewModels = {
    product: new product.ViewModel(),
    stock: new stock.ViewModel(),
    cart: new dora.Cart(),
    login: new account.Login(),
    register: new account.Register()
  };

  ko.validation.init({
    registerExtenders: true,
    messagesOnModified: true,
    insertMessages: false
  });

  ko.applyBindings(ViewModels);

  var login = new account.Login();
  login.user(currentUser);

  if (window.location.pathname === "/checkout/success.html") {
    cart.empty();
  }

});
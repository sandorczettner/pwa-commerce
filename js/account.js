"use strict";

define('account', [
        "jquery",
        "knockout",
        "knockout-validation",
        "parse"
        ],
function($, ko, ko_validation, Parse){
  var Account = Account || {};

  Account.spinner = ko.observable(false); // This is available for all property
  Account.user = ko.observable(new Object()); // This is available for all property
  /**
    Log In view model
  */
  Account.Login = function(options,callback) {
    var self = this;

    self.spinner = Account.spinner;
    self.user = Account.user;
    self.email = ko.observable("");
    self.password = ko.observable("");
    self.parseError = ko.observable("");
    self.parseMessage = ko.observable("");

    self.clickLogin = function() {
      self.spinner(true);
      self.parseMessage("");
      Parse.User.logIn(self.email(), self.password(), {
        success: function(user) {
          console.log(user);
          self.spinner(false);
          self.parseError("");
          self.user(user);
        },
        error: function(user, error) {
          self.parseError("Error: " + error.code + " " + error.message);
          self.spinner(false);
        }
      });
    };

    self.clickPassword = function() {
      self.spinner(true);
      self.parseMessage("");
      Parse.User.requestPasswordReset(self.email(), {
        success: function() {
          self.spinner(false);
          self.parseError("");
          self.parseMessage("Please open your email and click on the link I just sent you to reset your password.");
        },
        error: function(error) {
          self.parseError("Error: " + error.code + " " + error.message);
          self.spinner(false);
        }
      });
    };

    self.clickLogout = function() {
      Parse.User.logOut();
      self.user(null);
    };

    return self;
  };

  /**
    Sign Up view model
  */
  Account.Register = function(options,callback) {
    var self = this;

    self.spinner = Account.spinner;
    self.user = Account.user;
    self.email = ko.observable("").extend({ email: true, required: { message: 'Please supply your email address.' } });;
    self.password = ko.observable("");
    self.passwordVerify = ko.observable("").extend({ equal: self.password });
    self.newsletter = ko.observable(true);
    self.parseError = ko.observable("");
    self.errors = ko.validatedObservable(self);

    /**
     *
     */
    self.clickRegister = function() {
      var user = new Parse.User();
      self.spinner(true);
      user.set("username", self.email());
      user.set("password", self.password());
      user.set("email", self.email());
      user.signUp(null, {
        success: function(user) {
          console.log(user);
          self.spinner(false);
          self.parseError("");
          self.user(user);
        },
        error: function(user, error) {
          // Show the error message somewhere and let the user try again.
          self.parseError("Error: " + error.code + " " + error.message);
          self.spinner(false);
        }
      });
    }

    return self;
  };

  return Account;
});
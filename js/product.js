"use strict";

define('product', [
        "jquery",
        "knockout"
        ],
function($, ko){
  var Product = Product || {};

  Product.Option = function(options,callback) {
    var productOption = {};

    var sku = options.sku || [];
    var select = options.select || [];

    productOption.type = options.type || "";
    productOption.name = options.name || "";
    productOption.label = options.label || "";
    productOption.type = options.type || "";
    productOption.sku = ko.observableArray(sku);
    productOption.select = ko.observableArray(select);
    productOption.value = ko.observable("");

    if(callback){
      productOption.value.subscribe(function(value){
        callback.call(productOption,productOption,value);
      });
    }

    return productOption;
  }

  Product.ViewModel = function(options,callback) {
    var self = this;
    self.options = ko.observableArray();
    self.productData = ko.observableArray();

    self.selection = ko.computed(function() {
      var sku = "";
      var values = ko.utils.arrayMap(self.options(), function(option) {
        var ret = {};
        ret[option.name] = option.value();
        if (option.sku().length > 0 && option.select) {
          var i = option.select.indexOf(option.value());
          if (i != -1) {
            sku += "-" + option.sku()[i];
          }
        }
        return ret;
      })
      return {'selection': values, 'sku': sku}
    });

    self.onOptionsChange = function(option, value) {
      // nothing TODO
    };

    var optionsJSON = [];
    var productJSON = [];
    if ($("#product-options-json").length) {
      optionsJSON = $.parseJSON($("#product-options-json")[0].innerHTML);
    }
    self.options(ko.utils.arrayMap(optionsJSON, function(option) {
       return Product.Option(option, self.onOptionsChange);
    }));
    if ($("#product-data").length) {
      productJSON = $.parseJSON($("#product-data")[0].innerHTML);
    }
    self.productData(productJSON);

  };

  return Product;
});
"use strict";

define('stock', [
        "jquery",
        "knockout",
        "parse"
        ],
function($, ko, Parse){
  var Stock = Stock || {};

  Stock.ViewModel = function(options,callback) {
    var self = this;

    self.stock = ko.observableArray([]);

    self.skuQty = function(sku) {
      var Stock = Parse.Object.extend("Stock");
      var found = self.find(sku);
      if (found) {
        return found.qty;
      }

      // Stock was not in memory, try loading it.
      var query = new Parse.Query(Stock);
      query.equalTo("sku", sku);
      query.find({
        success: function(results) {
          // Do something with the returned Parse.Object values
          if (results.length == 0) {
            self.stock.push({
              qty: 0,
              sku: sku,
              inStock: false
            });
          }
          self.stock.push({
            qty: results[0].get('qty'),
            sku: sku,
            inStock: results[0].get('inStock')
          });
        },
        error: function(error) {
          alert("Error: " + error.code + " " + error.message);
        }
      });
    };

    self.find = function(sku) {
      return ko.utils.arrayFirst(self.stock(), function(child) {
          return child.sku === sku;
      });
    }
  };

  return Stock;
});